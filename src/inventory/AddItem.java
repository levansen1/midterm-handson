/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inventory;
import static inventory.Item.inventory;
import java.util.*;
/**
 *
 * @author levansen
 */
public class AddItem {
    Item newItem;
    private int quantity;
    public static Item[] inventory = new Item[100];
     private int itemCounter = 0;
     
     public AddItem(Item newItem){
         this.newItem = newItem;
         inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
     }

    public Item getNewItem() {
        return newItem;
    }

    public void setNewItem(Item newItem) {
        this.newItem = newItem;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public  Item[] getInventory() {
        return inventory;
    }

    public static void setInventory(Item[] inventory) {
        AddItem.inventory = inventory;
    }

    public int getItemCounter() {
        return itemCounter;
    }

    public void setItemCounter(int itemCounter) {
        this.itemCounter = itemCounter;
    }
     
     
     
}
